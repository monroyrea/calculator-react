import React from 'react';
import './styles/display.css'

class Display extends React.Component{
    constructor(props){
        super(props)
    };

    render(){
        return(
            <div id="display" className="display">
                <div className="row1">{this.props.secondaryDisplay}</div>
                <div className="row2">{this.props.mainDisplay}</div>
            </div>
        )
    }
}

export default Display;