import React from 'react';
import './styles/operationalbutton.css';

class OperationalButton extends React.Component{
    constructor(props){
        super(props);
    this.onClick = this.onClick.bind(this);
    };

    onClick = () =>{
        this.props.setValue(this.props.text);
    };

    render(){
        return(
            <button onClick={this.onClick} id={this.props.id} className="operational-button">
                {this.props.text}
            </button>
        )
    };
};

export default OperationalButton;