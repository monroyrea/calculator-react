import React from 'react';
import Display from './Display';
import OperationalButton from './OperationalButton';
import NumericalButton from './NumericalButton';
import Math from 'mathjs';
import './styles/calculatorcontainer.css'


class CalculatorContainer extends React.Component{
    constructor(props){
        super(props)
    this.state = {
        mainDisplay : "0",
        secondaryDisplay : "",
        total : 0
    }
    this.buttons = [{id:"zero", text:"0"},{id:"one", text:"1"},{id:"two", text:"2"},{id:"three", text:"3"},{id:"four", text:"4"},{id:"five", text:"5"},{id:"six", text:"6"},{id:"seven", text:"7"},{id:"eight", text:"8"},{id:"nine", text:"9"},{id:"decimal", text:"."}]
    this.setValue = this.setValue.bind(this);
    this.setOperation= this.setOperation.bind(this);
    }

    render(){
        return(
            <div className="calculator-container">
                <div className="screen">
                    <Display mainDisplay={this.state.mainDisplay} secondaryDisplay={this.state.secondaryDisplay}></Display>
                </div>
                <div className="button-pad">
                    {this.buttons.map((a,b) => {
                        return <NumericalButton key={a.id} id={a.id} text={a.text} setValue={this.setValue}></NumericalButton>
                    })}
                    <OperationalButton key={"clear"} id={"clear"} text={"AC"} setValue={this.setOperation}></OperationalButton>
                    <OperationalButton key={"equals"} id={"equals"} text={"="} setValue={this.setOperation}></OperationalButton>
                    <OperationalButton key={"add"} id={"add"} text={"+"} setValue={this.setOperation}></OperationalButton>
                    <OperationalButton key={"subtract"} id={"subtract"} text={"-"} setValue={this.setOperation}></OperationalButton>
                    <OperationalButton key={"multiply"} id={"multiply"} text={"*"} setValue={this.setOperation}></OperationalButton>
                    <OperationalButton key={"divide"} id={"divide"} text={"/"} setValue={this.setOperation}></OperationalButton>
                </div>
            </div>
        )
    }

    setValue = (value) => {
        let mainDisplay;
        let secondaryDisplay;
        const newVal = this.state.mainDisplay + value;
        if (newVal.length >15)
            return;

        switch(value){
            case ".":
                    if( this.state.secondaryDisplay.includes("=")){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: value, secondaryDisplay : value});
                    })
                    return;
                }
                if(!this.state.mainDisplay.match(/\./)){
                    mainDisplay = this.state.mainDisplay + value;
                    secondaryDisplay = this.state.secondaryDisplay + value;
                }
                if (this.state.mainDisplay.match(/[+\-\/*]$/)){
                    mainDisplay = "0" + value;
                    secondaryDisplay = this.state.secondaryDisplay + "0" + value;
                }
                break;
            case "0":
                if( this.state.secondaryDisplay.includes("=")){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: value, secondaryDisplay : value});
                    })
                    return;
                }
                if(this.state.mainDisplay !== "0"){
                    mainDisplay = this.state.mainDisplay +value;
                    secondaryDisplay = this.state.secondaryDisplay +value;
                }
                if (this.state.mainDisplay.match(/[+\-\/*]$/)){
                    mainDisplay = value;
                    secondaryDisplay = this.state.secondaryDisplay + value;
                }
                break;
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                if( this.state.secondaryDisplay.includes("=")){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: value, secondaryDisplay : value});
                    })
                    return;
                }
                if (!this.state.mainDisplay.match(/[+\/*]$/)){
                    mainDisplay = parseFloat(this.state.mainDisplay + value).toString();
                    secondaryDisplay = this.state.secondaryDisplay + value;
                }else{
                    mainDisplay = value;
                    secondaryDisplay = this.state.secondaryDisplay + value;
                }
                break;
            default:
                break;
        }
        if (!mainDisplay)
            mainDisplay = this.state.mainDisplay;
        if(!secondaryDisplay)
            secondaryDisplay = this.state.secondaryDisplay;
        this.setState(()=>{
            return Object.assign({}, this.state, {mainDisplay: mainDisplay, secondaryDisplay : secondaryDisplay});
        })
    };

    setOperation = (operation) =>{
        let match = [];
        switch(operation){
            case "=":
                let result =   Math.eval(this.state.secondaryDisplay);
                this.setState(()=>{
                    return Object.assign({}, this.state, {mainDisplay: result , secondaryDisplay : this.state.secondaryDisplay + "=" + result});
                })
                break;
            case "AC":
                this.setState(()=>{
                    return Object.assign({}, this.state, {mainDisplay: "0", secondaryDisplay : ""});
                })
                break;
            case "+":
            case "/":
            case "*":
                if(this.state.mainDisplay === "0")
                    return;
                
                if(this.state.secondaryDisplay.includes("=")){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.mainDisplay + operation });
                    })
                    break;
                }

                match = this.state.secondaryDisplay.match(/[+\-\/*]$/);
                if(!match){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.secondaryDisplay + operation});
                    })
                }else{
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.secondaryDisplay.replace(/[+\-\/*]$/, operation)});
                    }) 
                }
                break;
            case "-":
                if(this.state.mainDisplay === "0")
                    return;

                if(this.state.secondaryDisplay.includes("=")){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.mainDisplay + operation });
                    })
                    break;
                }
                
                match = this.state.secondaryDisplay.match(/[+\/*]$/);
                if(match){
                    this.setState(()=>{
                        return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.secondaryDisplay + operation});
                    })
                }else{
                    if(this.state.secondaryDisplay.match(/[\-]$/))
                        return;
                    else{
                        this.setState(()=>{
                            return Object.assign({}, this.state, {mainDisplay: operation, secondaryDisplay : this.state.secondaryDisplay + operation});
                        })
                    }

                }
                break;
            default:
                break;
            }
        }
}

export default CalculatorContainer;